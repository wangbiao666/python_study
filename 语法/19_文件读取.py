# 打开文件
import time

f = open("./test.txt", "r", encoding="UTF-8")
# 读取文件
# print(f.read(10))   # 读取10个字节
# print(f.read())     # 读取整个文件内容
# print(f.readlines())  # 读取整个的文件内容为一行
# print(f.readline())  # 只读取一行

# for循环读取
# for line in f:
#     print(line)  # 每次打印一行

# 文件关闭
# f.close()

# with 打开 读取完文件后，自动关闭文件
with open("./test.txt", "r", encoding="UTF-8") as ff:
    for line in ff:
        print(line)
