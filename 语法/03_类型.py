print(type("我有车贷和房贷"))
print(type(4144.66))
print(type(50))
# 执行后打印日志显示类型，打印的就是返回值


# 定义不同类型接收变量
string_type = type("我还有好多贷款")
float_type = type(4144.66)
int_type = type(50)

print(string_type)
print(float_type)
print(int_type)
