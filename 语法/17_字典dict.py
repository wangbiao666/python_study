# python 中的字典就是 map

# 定义字典
my_dict1 = {"王大狗": 100, "王二狗": 90, "王三狗": 80}
my_dict2 = {}
my_dict3 = dict()
print(my_dict1)
print(type(my_dict2))  # <class 'dict'>
print(type(my_dict3))  # <class 'dict'>

# 取值
print(my_dict1["王二狗"])  # 90

# 嵌套，value为字典
my_dict4 = {"my_dict1": my_dict1, "123": 123}
print(my_dict4["my_dict1"]["王二狗"])  # 90

# 新增元素,key不存在就是新增，存在就是更新
my_dict1["abiu"] = 120
my_dict1["王二狗"] = 120
print(my_dict1)  # {'王大狗': 100, '王二狗': 120, '王三狗': 80, 'abiu': 120}

# 删除
score = my_dict1.pop("王大狗")
print(score)  # 100
print(my_dict1)  # {'王二狗': 120, '王三狗': 80, 'abiu': 120}

# 取所有key
keys = my_dict1.keys()
print(keys)  # dict_keys(['王二狗', '王三狗', 'abiu'])

# 长度
print(len(my_dict1))  # 3

# 清空
my_dict1.clear()
print(my_dict1)  # {}
