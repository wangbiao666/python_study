import random

age = random.randint(1, 30)  # 去一个随机整数，范围 1 - 30
if age >= 18:
    print("我已经成年了")

print(age)  # 不管条件是否成立，这行都会打印

if age >= 18:
    print("准备去上大学")
elif age >= 20:  # 只能执行一个分支的内容，所以这个分支不会执行
    print("可以谈恋爱了")
else:
    print("想快点长大")
