# 元组和列表的区别就是，元素的元素不可修改，就是一个常量的列表，不可修改

# 定义元素
t1 = (1, 'hello', True)
t2 = ()  # 空元组
t3 = tuple()  # 也是空元组
print(t1)
print(t2)
print(t3)

# 定义单个元素的元组，必须要写个逗号，
t4 = ("hello",)
print(type(t4))  # <class 'tuple'>
print(t4)

# 元组也可以嵌套，二维元组
t5 = ((1, 2, 3), (4, 5, 6))
print(t5[1][1])  # 5

# 元组提供的常用方法和列表一样
# 元组.index()
print(t1.index(1))  # 0
# 元组.count()
print(t1.count('hello'))  # 1
# let(元组)
print(len(t1))  # 3

# 元组内的元素不可修改，如果这个元素是个列表，就可以修改列表里的元素
t6 = (1, 2, ["aaa", "bbb"])
t6[2][0] = "ccc"
t6[2][1] = "ddd"
print(t6)  # (1, 2, ['ccc', 'ddd'])
