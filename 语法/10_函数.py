# 定义函数
def my_len(data):
    count = 0
    for i in data:
        count += 1
    print(f"字符串：{data}\t的长度为：{count}")
    return count


s = "aBiu"
result = my_len(s)
print("结果是", result)


# 函数文档说明：
def func_test(x, y):
    """
    测试用的函数
    :param x:   第一个参数
    :param y:   第二个参数
    :return: 返回结果
    """
    return x + y


print(func_test(1, 1))  # 2


# 多返回值的函数
def func_two_return(x, y=5):  # 给y设置了默认值
    return x, y


# x, y = func_two_return(1, 1)
x, y = func_two_return(y=1, x=1)  # 如果通过关键字传参，可以不用按照函数的入参顺序
print(x)  # 1
print(y)  # 1


# 不定长参数的函数
def func_user(*args):
    print(args)  # (1, 2, 3)        类型是元组


func_user(1, 2, 3)


# # 不定长参数的函数，两个*
def func_user2(**kwargs):
    print(kwargs)  # {'name': '王二狗', 'age': 18}


# 必须传入键值对
func_user2(name="王二狗", age=18)


# 函数的参数是函数类型
def test_func(compute):
    result = compute(1, 2)
    print(result)


def compute(x, y):
    return x + y


test_func(compute)

# lambda 定义匿名函数
test_func(lambda x, y: x - y)   # lambda 不许只写一行，不可以换行
