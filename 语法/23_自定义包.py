# 导入自定义的包
# import test_package.my_module1
# import test_package.my_module2
#
# test_package.my_module1.info_print1()
# test_package.my_module2.info_print2()

# from 导入
# from test_package import my_module1
# from test_package import my_module2
# my_module1.info_print1()
# my_module2.info_print2()


# 第三种方式
# from test_package.my_module1 import info_print1
# from test_package.my_module2 import info_print2
# info_print1()
# info_print2()


# 测试init.py文件中的配置, 只能导入my_module1
from test_package import *
my_module1.info_print1()
