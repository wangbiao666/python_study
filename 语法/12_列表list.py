it_list = ['python', 'golang', 'java']
print(it_list)
print(type(it_list))  # <class 'list'>
print(it_list[0])  # python    不可超出下标索引，不然报错
print(it_list[-1])  # java      可以知道下标为负数，刚好相反

# 嵌套列表
num_list = [[1, 2, 3], [4, 5, 6]]
print(num_list[0][1])  # 2

# 列表常用方法：
print(it_list.index('python'))  # 0     如果传入不存在的元素，会报错，比如 it_list.index('aaa')

it_list.insert(2, "rust")  # 给指定下标位置插入元素，指定下标索引和插入的元素值
print(it_list)  # ['python', 'golang', 'rust', 'java']

it_list.append("c++")  # 在最后追加元素
print(it_list)

add_list = [1, 1, 1]
it_list.extend(add_list)  # 批量追加元素
print(it_list)

del it_list[3]  # 删除指定下标元素
print(it_list)
# 方式2：
element = it_list.pop(3)  # 不光可以删除，还可以接收删除的元素值
print(it_list)
print(element)
# 方式3：按下标顺序删除第一个指定的元素
it_list.remove(1)
print(it_list)

# it_list.clear()  # 清空列表
# print(it_list)  # []

# 统计列表同一个元素值的个数
count = it_list.count(1)  # 找有几个1
print(count)  # 2

# 列表长度
print(len(it_list))  # 5
