import json

data = [{"name": "王大狗", "age": 18}, {"name": "王二狗", "age": 20}, {"name": "王三狗", "age": 22}]
json_str = json.dumps(data, ensure_ascii=False)  # 如果不设置ensure_ascii=False，就是ascii乱码了
print(type(json_str))
print(json_str)

d = {"name": "王二狗", "age": 20}
json_str2 = json.dumps(d, ensure_ascii=False)
print(json_str2)

# json字符串转字典，要满足json格式
s = '{"name": "王二狗", "age": 20}'
l = json.loads(s)
print(l)
