# 定义集合  无序不重复，不支持索引
my_set = {1, 2, 3, 4, 5, 6, 1, 2}
print(type(my_set))  # <class 'set'>
print(my_set)  # {1, 2, 3, 4, 5, 6}

# 添加新元素
my_set.add("python")
print(my_set)   # {1, 2, 3, 4, 5, 6, 'python'}

# 移除元素
my_set.remove(1)
print(my_set)   # {2, 3, 4, 5, 6, 'python'}

# 随机取出元素
my_set_pop = my_set.pop()
print(my_set_pop)   # 随机的，因为是无序的

# 取出两个集合的差集
my_set2 = {1,2,7,8,9}
difference_set = my_set.difference(my_set2)
print(difference_set)   # {3, 4, 5, 6, 'python'}

# 消除两个集合的差集
my_set.difference_update(my_set2)
print(my_set)   # {3, 4, 5, 6, 'python'}
print(my_set2)  # {1, 2, 7, 8, 9}

# 合并集合
my_set3 = my_set.union(my_set2)
print(my_set3)  # {1, 2, 3, 4, 5, 6, 'python', 7, 8, 9}

# 集合长度
print(len(my_set3))     # 10

# 清空集合
my_set.clear()
print(my_set)   # set()

