# 打开命令行窗口，输入：pip install 【包名称】
# 就可以通过联网安装第三方包
# 比如：pip install -i https://pypi.tuna.tsinghua.edu.cn/simple/  numpy
# 这个写法是因为连的是国内的，pip install 【包名称】 默认是国外的，速度会非常慢