# 通过import导入模块
# import time
# time.sleep(5)  # 然后就可以使用
import time
# 只导入time模块中的sleep方法
# from time import sleep
# sleep(5)

# 使用 * 导入time模块的全部功能
# from time import *
# sleep(5)

# 使用as给特定功能加别名
import time as t
t.sleep(5)