# 对列表切片
my_list = [0, 1, 2, 3, 4, 5, 6]
result1 = my_list[1:4]
print(result1)  # [1, 2, 3]

# 对tuple切片
my_tuple = (0, 1, 2, 3, 4, 5, 6)
result2 = my_tuple[:]
print(result2)  # (0, 1, 2, 3, 4, 5, 6)

# 对字符串切片
my_str = "0123456"
result3 = my_str[::2]   # 从开始到结束，步长为2
result4 = my_str[::-1]   # 从开始到结束，步长为-1，等于反转
print(result3)      # 0246
print(result4)      # 6543210
