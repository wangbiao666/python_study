from pyecharts.charts import Map
from pyecharts.options import TitleOpts, LegendOpts, ToolboxOpts, VisualMapOpts

# 准备地图
map = Map()
# 数据准备
data = [
    ("北京", 99),
    ("上海", 199),
    ("湖南", 299),
    ("台湾", 399),
    ("广东", 499),
]
# 添加到地图
map.add("测试地图", data, "china")
# map.add("测试地图", data, "china")    # 比如要展示河南省地图

map.set_global_opts(
    # 设置标题
    title_opts=TitleOpts(title='全国疫情统计测试数据', pos_left='center', pos_bottom='1%'),
    # 设置图例
    legend_opts=LegendOpts(is_show=True),
    # 设置工具箱
    toolbox_opts=ToolboxOpts(is_show=True),
    # 视觉映射
    visualmap_opts=VisualMapOpts(
        is_show=True,
        is_piecewise=True,
        pieces=[
            {"min": 1, "max": 9, "label": "1-9", "color": "#CCFFFF"},
            {"min": 10, "max": 99, "label": "10-99", "color": "#FF6666"},
            {"min": 100, "max": 500, "label": "100-500", "color": "#990033"},
        ]
    )
)
map.render()
