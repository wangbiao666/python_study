name = "wang biao"
count = 0
# 字符串拆开
for s in name:
    if s == "a":
        count += 1

print(count)

# range语法：
# 写法1：
for i in range(10):  # 从0到10，不包含10
    print(i)  # 打印0-9

# 写法2：
for j in range(5, 10):  # 从5到10，不包含10
    print(j)  # 打印5-9

# 写法3：
for k in range(5, 10, 2):  # 从5到10，不包含10, 2个间隔顺序
    print(k)  # 打印5 7 9

# 练习：九九乘法表
for i in range(1, 10):
    for j in range(1, i + 1):
        print(f"{i} * {j} = {i * j} \t", end="")  # end="" 就可以换行了

    print()     // 换行
