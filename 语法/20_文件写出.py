# 打开文件，文件存在就打开，文件不存在会创建一个文件
# f = open("./test.txt", 'w')
# 模式为a，会追加文件内存
f = open("./test.txt", 'a')
# 写入内存缓冲区
f.write("aaaaaaaaaaaaaaaaaaaaaaa\n")
# 内容从缓冲区刷进磁盘，写入文件
f.flush()
# 关闭
f.close()
