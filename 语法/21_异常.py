# 捕获全部异常
try:
    i = 1 / 0
except:
    print("程序出错")

# 捕获指定异常
try:
    i = 1 / 0
except ZeroDivisionError as e:  # 如果异常类型不匹配，还是捕获不到异常
    print(e)  # division by zero

# 捕获多个异常，可能出现的异常
try:
    i = 1 / 0
except (NameError, ZeroDivisionError) as e:
    print(e)  # division by zero

# 捕获全部异常
try:
    i = 1 / 1
except:
    print("异常了")
else:  # 不出现异常就执行
    print("没有出现异常")
finally:  # 始终都会执行
    print("执行finally")
