# 把123变成字符串类型
num_str = str(123)
print(type(num_str), num_str)

str_test = "123"
int_num = int(str_test)
print(type(int_num), int_num)

float_num = float(str_test)
print(type(float_num), float_num)

# 这样就会报错:
# abc_num = int("abc")
