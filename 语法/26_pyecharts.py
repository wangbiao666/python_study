# echarts 图标，官网: pyecharts.org
# 命令行下载第三方包：pip install pyecharts

# 全局配置项，通过 set_global_opts 里面的选项


# 折线图
from pyecharts.charts import Line
from pyecharts.options import TitleOpts, LegendOpts, ToolboxOpts, VisualMapOpts

line = Line()
line.add_xaxis(['中国', '美国', '英国'])
line.add_yaxis('GDP', [30, 20, 10])

line.set_global_opts(
    # 设置标题
    title_opts=TitleOpts(title='GDP展示', pos_left='center', pos_bottom='1%'),
    # 设置图例
    legend_opts=LegendOpts(is_show=True),
    # 设置工具箱
    toolbox_opts=ToolboxOpts(is_show=True),
    # 视觉映射
    visualmap_opts=VisualMapOpts(is_show=True)
)
line.render()  # 生成一个render.html文件，如果指定名称，就可以修改文件名

